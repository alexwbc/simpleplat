extends KinematicBody2D

var state = "ground"
var state_new = "ground"


const RUN_SPEED = 200
const GRAVITY_VEC = Vector2(0, 2000)
#const JUMP_PWR = 800
const FLOOR_NORMAL = Vector2(0, -1)
const SNAP = Vector2(0,10)
var vel = Vector2()
var old_vel = Vector2()




var sprite_scale = 0



#AI
var dir = 0
var ai_state = "ai_patrol"
var ai_state_new = "ai_patrol"
var ai_delay = null

var unstuck_check = 0
#animations
var anim = "fall-loop"
var anim_new = "fall-loop"
var face_left = false
var face_left_new = false



#debug
var timer = 1
func _enter_tree():
	sprite_scale = $sprite.scale.x
	$sprite/atk.connect("body_entered",self,"detect_on_maw",[])

func _process(delta):
	
	$Label.text = str("state: ",state,"\nai_s: ",ai_state)
	
	
	call(ai_state,delta)
	if anim != anim_new:switch_anim()
	if face_left != face_left_new:switch_face()
	if ai_state != ai_state_new:switch_ai()

func ai_patrol_swtich_dir(delta):
	timer -= delta
	if timer <=0:
		ai_state_new = "ai_patrol"
		timer = 1
		if face_left:
			vel.x = RUN_SPEED
			dir = 1
		else:
			vel.x = -RUN_SPEED
			dir = -1
		face_left_new = !face_left_new
		switch_face()



func ai_patrol(delta):#AI process for patrol
	if anim == "atk":print("error!")
	if abs(vel.x)<80:
		unstuck_check -= delta
		if unstuck_check <=0:
			ai_state_new = "ai_patrol_swtich_dir"
			return
	else:unstuck_check = 0.3
	$sprite/checkground.force_raycast_update()
	if !$sprite/checkground.is_colliding():
		ai_state_new = "ai_patrol_swtich_dir"


func _physics_process(delta):
	vel += delta * GRAVITY_VEC

	if state != state_new:switch_state()
	call(state,delta)
#
	old_vel = vel



func ground(delta):
	if ai_state == "ai_patrol_swtich_dir":
		anim_new = "idle"
		vel = Vector2(0,0)
		return
	vel = move_and_slide_with_snap(vel,SNAP,FLOOR_NORMAL)#,false,4, 0)

	var target_speed = dir*RUN_SPEED
	vel.x = lerp(vel.x, target_speed, 0.3)
	if abs(vel.x)<76:
		anim_new = "idle"
	elif dir != 0:
		face_left_new = (vel.x<0)
		anim_new="walk"



func switch_anim():
	$anim.play(anim_new)
	anim = anim_new
func switch_face():
#	print("switching face")
	if face_left_new:
		$sprite.scale.x = -sprite_scale
	else:
		$sprite.scale.x = sprite_scale
	face_left = face_left_new
func switch_state():
#	print("switch state: ",state_new)

	state = state_new
func switch_ai():
	unstuck_check = 1
#	print("switch ai to: ",ai_state_new)
	ai_state = ai_state_new


func attack(delta):
	if !$anim.is_playing():# or $anim.current_animation != "atk":
		state_new = "ground"
		ai_state_new = "ai_patrol"

	
func ai_wait(delta):
	if ai_delay != state_new:
		ai_state_new = "ai_patrol"
		anim_new = "idle"

func ai_behit(delta):
	pass
func detect_on_maw(what):
	if what.immunity != null:return#don't attak player on its grace period
	if ai_state == "ai_wait":return
	
	anim_new = "atk"
	ai_state_new = "ai_wait"
	ai_delay = "attack"
	state_new = "attack"
	var direction = -1
	if what.global_position.x>global_position.x:direction = 1
	
#	face_left_new =  (direction == 1)
	face_left_new =  (direction == -1)
	dir = direction
	switch_face()
	what.got_hit("tyrbite",direction)

func behit(delta):
	vel = move_and_slide(vel,FLOOR_NORMAL)
	if !$anim.is_playing() and $sprite/checkground.is_colliding():
		state_new = "ground"
		ai_state_new = "ai_patrol"

func got_hit(from_what, direction):
	if state == "behit":return
	anim_new = "behit"
	ai_state_new = "ai_behit"
	state_new = "behit"
#	switch_state()
	if direction:
		face_left_new = true
		vel.x = 100
	else:
		face_left_new = false
		vel.x = -100
	vel.y = -100
