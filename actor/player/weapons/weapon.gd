extends Area2D

var float_pos
var throw_pos
var melee_pos

var pos = [Vector2(),Vector2()]
var player
var reach
var state = "wfloat"

var go_left = false

var mode = "spear"
var mode_req = "dagger"
var models = {
	"w_2hsword":preload("res://actor/player/weapons/w_2hsword.png")
	,"w_axe_unknow":preload("res://actor/player/weapons/w_axe_unknow.png")
	,"dagger":preload("res://actor/player/weapons/w_dagger02.png")
	,"w_halberd":preload("res://actor/player/weapons/w_halberd.png")
	,"w_katana_diamond":preload("res://actor/player/weapons/w_katana_diamond.png")
	,"w_katana_fire":preload("res://actor/player/weapons/w_katana_fire.png")
	,"w_katana_holly":preload("res://actor/player/weapons/w_katana_holly.png")
	,"w_katana":preload("res://actor/player/weapons/w_katana.png")
	,"w_katana_wooden":preload("res://actor/player/weapons/w_katana_wooden.png")
	,"w_longsword02":preload("res://actor/player/weapons/w_longsword02.png")
	,"w_mace_copper":preload("res://actor/player/weapons/w_mace_copper.png")
	,"w_mace_dark":preload("res://actor/player/weapons/w_mace_dark.png")
	,"w_mace_diamond":preload("res://actor/player/weapons/w_mace_diamond.png")
	,"w_mace_fire":preload("res://actor/player/weapons/w_mace_fire.png")
	,"w_mace_holly":preload("res://actor/player/weapons/w_mace_holly.png")
	,"w_mace_ice":preload("res://actor/player/weapons/w_mace_ice.png")
	,"w_mace":preload("res://actor/player/weapons/w_mace.png")
	,"w_sabre":preload("res://actor/player/weapons/w_sabre.png")
	,"w_shortsword02":preload("res://actor/player/weapons/w_shortsword02.png")
	,"w_spear":preload("res://actor/player/weapons/w_spear.png")
	,"w_sword_roman_blood":preload("res://actor/player/weapons/w_sword_roman_blood.png")
	,"w_sword_roman_dark":preload("res://actor/player/weapons/w_sword_roman_dark.png")
	,"w_sword_roman_diamond":preload("res://actor/player/weapons/w_sword_roman_diamond.png")
	,"w_sword_roman_holly":preload("res://actor/player/weapons/w_sword_roman_holly.png")
	,"w_sword_roman_lightning":preload("res://actor/player/weapons/w_sword_roman_lightning.png")
	,"w_sword_roman":preload("res://actor/player/weapons/w_sword_roman.png")
	,"w_sword_roman ruby":preload("res://actor/player/weapons/w_sword_roman ruby.png")
	,"trident":preload("res://actor/player/weapons/w_trident.png")
}
var weapon_weight = {"dagger":0.2, "trident":0.5}
var m_index =[]

var fade_script = preload("res://actor/player/weapons/weapon_fade.gd")


signal weapon_ready
signal weapon_busy

var timer = 0
var switch = false
func _ready():
#	connect("weapon_ready",player,"weapon_reach")
	
	connect("weapon_ready",player,"set",["weapon_ready2cast",true])
	connect("weapon_busy",player,"set",["weapon_ready2cast",false])
	$anim.connect("animation_finished",self,"animation_finished",[])
#	weapon_ready2cast
#	connect("weapon_ready",player,"weapon_busy")

func animation_finished(what):
	if what == "comeup":
		emit_signal("weapon_ready")
		$sprite.scale = Vector2(1,1)
		$anim.play("float")
	if what =="return_float":
		emit_signal("weapon_ready")
		$sprite.scale = Vector2(1,1)
		$anim.play("float")
		state = "wfloat"
		

	for m in models:
		m_index.append(m)


#	emit_signal("weapon_ready")

func _physics_process(delta):
	call(state,delta)
	
func _process(delta):
#	print()
	if mode != mode_req:switch_weapon()
	if Input.is_action_just_pressed("ui_accept"):
		if mode == "dagger":
			mode_req = "trident"
#			emit_signal("weapon_ready")
		else:
#			emit_signal("weapon_busy")
			mode_req = "dagger"
			

func switch_weapon():
	if !player.weapon_ready2cast:return
	emit_signal("weapon_busy")
	var deadsprite = $sprite.duplicate()
	deadsprite.set_script(fade_script)
	add_child(deadsprite)
	$sprite.texture = models[mode_req]
	$sprite.modulate.a = 0
	mode = mode_req
	$anim.play("comeup")
	player.weapon_weight = weapon_weight[mode]
	
	if mode_req == "dagger":
		$dagger.disabled = false
		$trident.disabled = true
	if mode_req == "trident":
		$dagger.disabled = true
		$trident.disabled = false
	print("req: ",mode_req )

func cast(delta):
#	print("collide with: ",get_overlapping_bodies())
	var to_return = false
	if go_left:global_position.x -= 600*delta
	else:global_position.x += 600*delta

	reach += 1*delta
	if reach >=1:
		to_return= true

#	if get_overlapping_areas() != []:
#		for i in get_overlapping_areas():
#			if i.name == "hitbox":
#				i.get_parent().get_parent().got_hit("dagger",i.global_position.x>global_position.x)
#				to_return = true

	if check_hit():
		to_return = true
	if get_overlapping_bodies() != []:
		to_return = true
	
	if to_return:
		$sprite.modulate.a = 0.3
		reach = 0
		$anim.play("float")
		state = "returning"
		pos = [global_position,throw_pos.global_position]
	
func throw_weapon(left):
	if mode == "dagger":throw_dagger(left)
	elif mode == "trident":throw_trident(left)

func throw_trident(left):
	if left:scale.x = -1
	else:scale.x = 1

	$anim.play("spear_atk")
	pos = [global_position,melee_pos.global_position]
	state ="trident_atk"
	reach = 0

func trident_atk(delta):#here
	if $anim.current_animation == "spear_atk":
		if get_overlapping_areas() != []:check_hit()
#			for i in get_overlapping_areas():
#				if i.name == "hitbox":
#					var enemy = i.get_parent().get_parent()
#					if enemy.state != "behit":
#						enemy.got_hit("trident",i.global_position.x>global_position.x)
#						var impact_pos = global_position.linear_interpolate(enemy.global_position,0.5)
#						base.shootfx(impact_pos)

	reach += 10*delta
	if reach <1:global_position = lerp(pos[0],pos[1],reach)


func check_hit():
	if get_overlapping_areas() != []:
		for i in get_overlapping_areas():
			if i.name == "hitbox":
				var enemy = i.get_parent().get_parent()
				if enemy.state != "behit":
					enemy.got_hit(mode,i.global_position.x>global_position.x)
					var impact_pos = global_position.linear_interpolate(enemy.global_position,0.5)
					base.shootfx(impact_pos)
					return true
	return false
func throw_dagger(left):
	go_left = left
	
#	if left:$anim.play("atk_l")
#	else:$anim.play("atk_r")
	if left:scale.x = -1
	else:scale.x = 1
	$anim.play("atk")
	state = "throw"
	pos = [global_position,throw_pos.global_position]
	reach = 0

func throw(delta):
	reach += 10*delta
	global_position = lerp(pos[0],pos[1],reach)
	if reach >=1:
		reach =0
		state = "cast"

func returning(delta):
	reach += 2*delta
	global_position = lerp(pos[0],float_pos.global_position,reach)
	if reach >=1:
		scale.x = 1
		$sprite.modulate.a = 1
		state = "wfloat"
#		print("sending signal")
		emit_signal("weapon_ready")
#		var mname = m_index[randi()%m_index.size()]
#		$sprite.texture = models["_trident"]
#		$sprite.texture = models["2handsword"]

func wfloat(delta):
	var target_pos = float_pos.global_position-global_position
	if target_pos.length() > 2:
		global_position += target_pos*delta*10
