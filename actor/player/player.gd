extends KinematicBody2D

#
var state = "falling"
var state_new = "falling"

var stagger = null
var stagger_delay = 0
const RUN_SPEED = 250
const GRAVITY_VEC = Vector2(0, 2000)
const JUMP_PWR = 800
const FLOOR_NORMAL = Vector2(0, -1)
const SNAP = Vector2(0,10)
var vel = Vector2()
var old_vel = Vector2()
var plug_vel = null


#jump dynamics
var was_on_ground = 0
const GROUND_DELAY = 0.04


#animations
var anim = "fall-loop"
var anim_new = "fall-loop"
var face_left = false
var face_left_new = false

var sprite_scale = 0


#debug and other useless stuff
var checkpower = false


var weapon = preload("res://actor/player/weapons/weapon.tscn")
#signals
#
signal throw_weapon
var weapon_ready2cast = true
var weapon_weight = 0.2#used for stagger


#getting hit by enemies
var immunity = null#grace time in which player can't be damaged
var bounce = Vector2()
const DEFAULT_IM_PERIOD = 1

func _ready():
	_set_weapon()

func _set_weapon():
	var weap = weapon.instance()
	weap.global_position = global_position
#	var weapon = load("res://actor/player/weapons/dagger_debug.tscn").instance()
#	weapon.float_pos = $sprite/positions/weapon_float
#	weapon.throw_pos = $sprite/positions/weapon_atk
#	weapon.player = self
#	connect("throw_weapon",weapon,"throw_weapon",[])

	weap.float_pos = $sprite/positions/weapon_float
	weap.throw_pos = $sprite/positions/weapon_atk
	weap.melee_pos = $sprite/positions/weapon_mele
	weap.player = self
	connect("throw_weapon",weap,"throw_weapon",[])
	call_deferred("reparent",weap)


func reparent(node):
#	add_collision_exception_with(node)
	get_parent().add_child(node)


func _enter_tree():
	sprite_scale = $sprite.scale.x
#
#
func _process(delta):
	if Input.is_action_just_released("ui_accept"):checkpower = !checkpower

	if Input.is_action_just_pressed("ui_page_up"):$cam.zoom *=1.1
	elif Input.is_action_just_pressed("ui_page_down"):$cam.zoom *=0.9

	if anim != anim_new:
		switch_anim()
	if face_left != face_left_new:switch_face()

	if immunity != null:
		var alpha = lerp($sprite.modulate.a,randf(),0.3)
		$sprite.modulate.a = alpha
		immunity -= delta
		if immunity <= 0:
			$sprite.modulate.a = 1
			immunity = null

func switch_face():
	if face_left_new:
		$sprite.scale.x = -sprite_scale
	else:
		$sprite.scale.x = sprite_scale
	face_left = face_left_new

func get_impact():
	var angle = $checkground.get_collision_normal().angle()+1.57
	var impact = 0
	if face_left:
		if angle > 0.7:impact = 2
		elif angle > 0.46: impact = 1
	else:
		if angle < -0.7:impact = 2
		elif angle < -0.46: impact = 1
	return impact
#
func switch_anim():
	$anim.play(anim_new)
	anim = anim_new
#
func _physics_process(delta):
	if stagger != null:
		$state.set_text("stagger")
		if stagger_delay == null:
			if $anim.is_playing():return
			stagger_delay = 0
		stagger_delay -= delta
		if stagger_delay <= 0:
			if state != stagger:
				state_new = stagger
				switch_state()
			stagger = null
			stagger_delay = 0
			vel = Vector2()
			$state.set_text(state)
		return
	vel += delta * GRAVITY_VEC
#
	call(state,delta)

	if state != state_new:switch_state()
	old_vel = vel

func get_dir():
	return Input.get_action_strength("move_right")-Input.get_action_strength("move_left")

func atk_1(delta):
	
#	face_left_new = (vel.x<0)
	if face_left != face_left_new:switch_face()
	vel.x =0
	if !$anim.is_playing():
		state_new = "onground"
		vel = Vector2()

func duck(delta):
	vel = move_and_slide_with_snap(vel,SNAP,FLOOR_NORMAL)
	vel.x = lerp(vel.x, 0, 0.1)
	if !$checkground.is_colliding():state_new = "falling"
	if !Input.is_action_pressed("duck"):
		anim_new = "standup"
		switch_anim()
		anim_new= "idle"
		anim = "idle"
		state_new = "onground"

func switch_state():
	if state == "onground":was_on_ground = GROUND_DELAY#everytime we abandon the ground, ground delay apply
	state = state_new
	$state.set_text(state)
#
func jump(delta):
	was_on_ground-=delta
	var dir = Input.get_action_strength("move_right")-Input.get_action_strength("move_left")


	vel = move_and_slide(vel,FLOOR_NORMAL)
	var target_speed = dir*RUN_SPEED
	vel.x = lerp(vel.x, target_speed, 0.1)
	if $checkground.is_colliding() and (was_on_ground<=0) or vel.y >0:
		anim_new = "jump2fall"
		switch_anim()
		anim = "fall-loop"
		anim_new = "fall-loop"
		state_new = "falling"
		return
	if Input.is_action_just_pressed("atk"):
		air_atk()



func air_atk():

	if !weapon_ready2cast:return
	if vel.y < 0:vel.y = 0
	var dir = get_dir()
	if dir != 0:
		face_left_new = dir<0
		switch_face()
	anim_new = "atk-air"
	stagger = "falling"
#	stagger_delay = 0.2
	stagger_delay = weapon_weight 
	

	emit_signal("throw_weapon",face_left)
	weapon_ready2cast = false
	state_new = "atk_1"
	

func falling(delta):

	var dir = Input.get_action_strength("move_right")-Input.get_action_strength("move_left")

#	if face_left:dir = min(dir,0.5)
#	else:dir = max(dir,-0.5)

	vel = move_and_slide(vel,FLOOR_NORMAL)

	var target_speed = dir*RUN_SPEED
	var v_speed = vel.y

	if v_speed >0:anim_new="fall-loop"
	vel.x = lerp(vel.x, target_speed, 0.1)
	if $checkground.is_colliding():
		if abs(vel.x) < 10 or (get_impact() > 0):
			anim_new = "land_stand"
			switch_anim()
			anim_new = "idle"
			anim = "idle"
			vel = Vector2()
			stagger = "onground"
			stagger_delay = 0.1
		state_new = "onground"

	if Input.is_action_just_pressed("atk"):
		air_atk()

#		if get_impact() > 0:
#			vel.x = 0



func getdamaged(delta):
#	if vel.x > 0:face_left_new = true
	anim_new = "jump2fall"
	face_left_new = vel.x > 0
#	print(face_left_new)
	if bounce != null:#phase 1
		vel.x *= 0.2
		
		if abs(vel.x) <=0.1:
			vel = bounce
			bounce = null
		return
		
		
	was_on_ground-=delta
#	print(was_on_ground)
	vel = move_and_slide(vel,FLOOR_NORMAL)
	if $checkground.is_colliding() and (was_on_ground<=0):
		state_new = "onground"

func onground(delta):
#	var dir = Input.get_action_strength("move_right")-Input.get_action_strength("move_left")
	var dir = get_dir()
	vel = move_and_slide_with_snap(vel,SNAP,FLOOR_NORMAL)#,false,4, 0)

	var target_speed = dir*RUN_SPEED
	vel.x = lerp(vel.x, target_speed, 0.3)


	if abs(vel.x)<76:
		anim_new = "idle"
	elif dir != 0:
		face_left_new = (vel.x<0)
		anim_new="run"


	if !$checkground.is_colliding():
		was_on_ground-=delta
		if was_on_ground<=0:
			state_new = "falling"
			return
	else:was_on_ground = GROUND_DELAY

	if Input.is_action_just_pressed("jump"):
		state_new = "jump"
		anim_new = "jump-loop"
		vel.y = -JUMP_PWR
		switch_anim()
	elif Input.is_action_pressed("duck"):
		state_new = "duck"
		anim_new = "duck"
	elif Input.is_action_pressed("atk") and weapon_ready2cast:
		if dir != 0:
			face_left_new = dir<0
			switch_face()
		emit_signal("throw_weapon",face_left)
		weapon_ready2cast = false
		state_new = "atk_1"
		anim_new = "atk_1"

func got_hit(how,fromwhere):
	if immunity != null:return
	if state != "special":
		was_on_ground = 0.5
		state_new = "getdamaged"
		immunity = 1
		bounce = Vector2(100,-400)
		vel.x = 100
		if fromwhere <0:
			bounce.x = -100
			vel.x = -100
